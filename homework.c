#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef struct{
    char r;
    char g;
    char b;
}pixel;
typedef struct {
    int P;
    int width;
    int height;
    int maxval;
    pixel** color;
    char** bw;

}image;

void readInput(const char * fileName, image *img) {
    FILE* fisier= fopen(fileName,"rb");
    fscanf(fisier,"P%d\n",&(img->P) );
    fscanf(fisier,"%d %d\n",&(img->width),&(img->height));
    fscanf(fisier,"%d\n",&(img->maxval));
    int i,j;
    if( img->P == 5){
        img->bw=(char**)malloc(img->height*sizeof(char*));
        for(i=0;i<img->width;i++)
            img->bw[i]=(char*)malloc(img->width*sizeof(char));
           
        for(i=0;i<img->height;i++){
            for(j=0;j<img->width;j++)
                fread(&(img->bw[i][j]),1,1,fisier);
        }
    }
    else{
        img->color=(pixel**)malloc(img->height*sizeof(pixel*));
        for(i=0;i<img->width;i++)
            img->color[i]=(pixel*)malloc(img->width*sizeof(pixel));
        for(i=0;i<img->height;i++){
            for(j=0;j<img->width;j++)
                fread(&(img->color[i][j]),sizeof(pixel),1,fisier);
        }

    }
    fclose(fisier);

}

void writeData(const char * fileName, image *img) {
    FILE* fisier= fopen(fileName,"wb");
    fprintf(fisier,"P%d\n",(img->P) );
    fprintf(fisier,"%d %d\n",(img->width),(img->height));
    fprintf(fisier,"%d\n",(img->maxval));
    int i, j;
    puts("aci");
    if(img->P == 5){
        for(i=0;i<img->height;i++){
            for(j=0;j<img->width;j++)
                fwrite(&(img->bw[i][j]),1,1,fisier);
        }
    }
    else{
  
        for(i=0;i<img->height;i++){
            for(j=0;j<img->width;j++){
                fwrite(&(img->color[i][j]),sizeof(pixel),1,fisier);
            }

        }

    }
    fclose(fisier);
}

int main(int argc, char * argv[]) {

    image input;
    readInput(argv[1], &input);
    puts("scriu");
    writeData(argv[2], &input);
}